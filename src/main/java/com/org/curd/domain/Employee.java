package com.org.curd.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Document("employee")
@Getter
@Setter
@AllArgsConstructor
public class Employee {

	@Id
	private Long id;
	private String name;
	private String role;
	private String description;
}
