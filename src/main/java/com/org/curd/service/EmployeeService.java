package com.org.curd.service;

import java.util.List;

import com.org.curd.domain.Employee;

public interface EmployeeService {

	Employee saveEmployeeData(Employee employee);

	Employee updateEmployeeData(Employee employee);

	void deleteEmployeeData(Long id);

	List<Employee> getAllEmployeeData();

	Employee getEmployeeData(Long id);

}
