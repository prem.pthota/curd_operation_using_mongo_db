package com.org.curd.service.impl;

import java.lang.module.ResolutionException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.curd.domain.Employee;
import com.org.curd.repository.EmployeeRepository;
import com.org.curd.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public Employee saveEmployeeData(Employee employee) {
		// TODO Auto-generated method stub
		return employeeRepository.save(employee);
	}

	@Override
	public Employee updateEmployeeData(Employee employee) {

		Optional<Employee> eOptional = employeeRepository.findById(employee.getId());
		if (eOptional.isPresent()) {
			eOptional.get().setId(employee.getId());
			eOptional.get().setName(employee.getName());
			eOptional.get().setDescription(employee.getRole());
			eOptional.get().setDescription(employee.getDescription());
			return employeeRepository.save(eOptional.get());
		} else {
			throw new ResolutionException("Record not found with employee id : " + employee.getId());
		}
	}

	@Override
	public void deleteEmployeeData(Long id) {
		// TODO Auto-generated method stub

		Optional<Employee> eOptional = employeeRepository.findById(id);
		if (eOptional.isPresent()) {
			this.employeeRepository.delete(eOptional.get());
		} else {
			throw new ResolutionException("Record not found with employee id : " + id);
		}

	}

	@Override
	public List<Employee> getAllEmployeeData() {
		// TODO Auto-generated method stub
		return employeeRepository.findAll();
	}

	@Override
	public Employee getEmployeeData(Long id) {
		// TODO Auto-generated method stub
		Optional<Employee> eOptional = employeeRepository.findById(id);
		if (eOptional.isPresent()) {
			return eOptional.get();
		} else {
			throw new ResolutionException("Record not found with employee id : " + id);
		}
	}

}
