package com.org.curd.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.curd.domain.Employee;
import com.org.curd.service.EmployeeService;

@RestController
@RequestMapping({ "/curd" })
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@PostMapping("/save")
	public ResponseEntity<Employee> saveEmployeeData(@RequestBody Employee employee) {
		return ResponseEntity.ok(employeeService.saveEmployeeData(employee));
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Employee> updateEmployeeData(@RequestBody Employee employee) {
		return ResponseEntity.ok(employeeService.updateEmployeeData(employee));
	}

	@GetMapping("/get/{id}")
	public ResponseEntity<Employee> getEmployeeData(@PathVariable Long id) {
		return ResponseEntity.ok(employeeService.getEmployeeData(id));
	}

	@GetMapping("/get/all")
	public ResponseEntity<List<Employee>> getAllEmployeeData() {
		return ResponseEntity.ok(employeeService.getAllEmployeeData());
	}

	@DeleteMapping("/delete/{id}")
	public void deleteEmployeeData(@PathVariable Long id) {
		employeeService.deleteEmployeeData(id);
		ResponseEntity.status(HttpStatus.OK);
	}

}
